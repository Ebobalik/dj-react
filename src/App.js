import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import TracksListPage from './pages/TracksListPage';
import {BrowserRouter,Route,Routes} from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header></Header>
      <BrowserRouter>
      <Routes>
        <Route path='/' element={<TracksListPage/>}></Route>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
