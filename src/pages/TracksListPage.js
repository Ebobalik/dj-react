import React, { useEffect, useState } from 'react'
import { DataGrid } from '@mui/x-data-grid';
import { Box, Button, Typography } from '@mui/material';
import axios from 'axios';
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
const api_url = "http://127.0.0.1:5000/";

async function fetchTracks(){
    try {
        const response = axios.get(api_url+"fetch_tracks");
        return (await response).data;
      } catch (error) {
        return (await error).response.data;
      }
}

function TracksListPage() {
    const [NeedReload,SetReload] = useState(false);
    const navigate = useNavigate();
    const columns = [
        {field:'id',headerName:"ID"},
        {field:'name',headerName:"Название",width:600},
        {field:"type",headerName:"Сервис",width:250},
        {field:'url',headerName:"",width:200,
        renderCell: (cellValues) =>{
            return <Link style={{ textDecoration: 'none' }} to={cellValues.value} target='_blank'><Button variant='contained'>Открыть</Button></Link>;
        }
        },
        {field:'btn',headerName:"",width:200,
        renderCell: (cellValues) =>{
            return <Link to="/"><Button color='error' variant='contained' onClick={()=>{axios.get(api_url+"delete_track/",{params:{url:cellValues.value}});window.location.reload()}}>Удалить</Button></Link>;
        }
        },
    ]
    const [Getted,SetGetted]= useState(false);
    const [tracks,SetTracks] = useState([]);
    useEffect(()=>{
        if (Getted===false){
        fetchTracks().then((res)=>{
            var result = [];
            if(true){
                for(let i=0;i<res.length;i++){
                    let temp = {id:i,name:res[i]['name'],type:(res[i]['type']==="yandex"? "Яндекс Музыка":"YouTube"),url:res[i]['url'],print_url:res[i]['url'],btn:res[i]['url']};
                    result.push(temp);
                }
                SetTracks(result);
            }
        })
        SetGetted(true);
    }
    },[Getted,NeedReload]);

    return (
        <Box sx={{marginLeft:"50px",marginRight:"50px",marginTop:"20px"}}>
        <Typography variant='h5'>Очередь треков</Typography>
        <DataGrid
            sx={{marginTop:"20px"}}
            columns={columns}
            rows={tracks}
            disableRowSelectionOnClick
        >
        </DataGrid>
        </Box>
    )
}

export default TracksListPage;